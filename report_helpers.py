import numpy as np
import matplotlib.pyplot as plt 

def print_samples(labal, X_train, Y_train):
    """
    create png file with 9 sample of given digit 

    Arguments:
    labels -- label to print
    X_train -- the images dataset to be filtered
    Y_train -- the labels of the images

    """
    idxlist = []
    for idx, elm in np.ndenumerate(Y_train):
        if elm in labal:
            idxlist.append(idx[0])
    idxlist.sort()
    
    f, axarr = plt.subplots(1,9)
    image1 = X_train[:,idxlist[0]].reshape((28,28))
    image2 = X_train[:,idxlist[1]].reshape((28,28))
    image3 = X_train[:,idxlist[2]].reshape((28,28))
    image4 = X_train[:,idxlist[3]].reshape((28,28))
    image5 = X_train[:,idxlist[4]].reshape((28,28))
    image6 = X_train[:,idxlist[5]].reshape((28,28))
    image7 = X_train[:,idxlist[6]].reshape((28,28))
    image8 = X_train[:,idxlist[7]].reshape((28,28))
    image9 = X_train[:,idxlist[8]].reshape((28,28))
    axarr[0].imshow(image1)
    axarr[0].axis('off')
    axarr[1].imshow(image2)
    axarr[1].axis('off')
    axarr[2].imshow(image3)
    axarr[2].axis('off')
    axarr[3].imshow(image4)
    axarr[3].axis('off')
    axarr[4].imshow(image5)
    axarr[4].axis('off')
    axarr[5].imshow(image6)
    axarr[5].axis('off')
    axarr[6].imshow(image7)
    axarr[6].axis('off')
    axarr[7].imshow(image8)
    axarr[7].axis('off')
    axarr[8].imshow(image9)
    axarr[8].axis('off')
    f.savefig(str(labal[0])+'.png')

    return None