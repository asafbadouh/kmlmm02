from helper_MNIST import *
import numpy as np
import matplotlib.pyplot as plt
import math

def normalize_data(images):
    # Subtract mean of each image from its individual values
    mean = images.mean(axis=0)
    images = images - mean

    # Truncate to +/- 3 standard deviations and scale to -1 and +1
    pstd = 3 * images.std()
    images = np.maximum(np.minimum(images, pstd), -pstd) / pstd

    # Rescale from [-1,+1] to [0,1]
    images = (1 + images) # 0.4 + 0.1

    return images


def L_layer_model(X, 
                  Y, 
                  layers_dims,
                  learning_rate_value,                    # if you chose learning rate fixed then specify the learning rate
                  num_iterations,                         # number of iterations to perform
                  learning_rate_type = None,              # type of dynamic decrease of learning rate
                  learning_rate_decrease_rate = None,     # STEP DECAY: chose quantity to gradually decrease lr
                  learning_rate_interval_decrease = None, # STEP DECAY: chose the interval to low the lr
                  starting_learning_rate = None,          # EXPONENTIAL DECAY/(1/t): this is alpha0
                  k = None,                               # EXPONENTIAL DECAY/(1/t): this is the exponent k                  
                  hidden_activation = "relu",             # choose activiation function for hidden layers: "relu", "sigmoid" or "tanh"
                  print_cost=False):#lr was 0.009
    """
    Implements a L-layer neural network: [LINEAR->RELU]*(L-1)->LINEAR->SIGMOID.
    
    Arguments:
    X -- data, numpy array of shape (number of examples, num_px * num_px * 3)
    Y -- true "label" vector (containing 0 if cat, 1 if non-cat), of shape (1, number of examples)
    layers_dims -- list containing the input size and each layer size, of length (number of layers + 1).
    learning_rate -- learning rate of the gradient descent update rule
    num_iterations -- number of iterations of the optimization loop
    print_cost -- if True, it prints the cost every 100 steps
    
    Returns:
    parameters -- parameters learnt by the model. They can then be used to predict.
    """

    np.random.seed(1)
    
    costs = {}                         # keep track of cost
    lr = {}                            # keep track of learning rate evolution
    lr[0] = learning_rate_value        # learning rate at first iteration
    
    # Parameters initialization. (≈ 1 line of code) 
    parameters = initialize_parameters_deep(layers_dims) 

    # Loop (gradient descent)
    for i in range(0, num_iterations):
        
        # Forward propagation: [LINEAR -> RELU]*(L-1) -> LINEAR -> SIGMOID. 
        AL, caches = L_model_forward(X, parameters, hidden_activation) 
         
        # Compute cost. 
        cost = compute_cost(AL,Y) # binary cross
                
        # Backward propagation. 
        grads = L_model_backward(AL,Y,caches, hidden_activation) 
 
        # Update parameters. 
        parameters = update_parameters(parameters, grads, learning_rate_value) 
                
        # Print the cost every 100 training example
        if print_cost and i % 50 == 0:
            print ("Cost after iteration %i: %f" %(i, cost))
        if print_cost and i % 50 == 0:
            #costs.append(cost)
            costs[i] = cost
        
        '''
        In case of dynamic learning rate these are the possible cases
        '''
        
        # Implement here the Learning rate decay
        if learning_rate_type == "SD":
            if i % learning_rate_interval_decrease == 0:
                # low the learning rate
                learning_rate_value -= (learning_rate_value*learning_rate_decrease_rate)
                # save lr value in dictonary
                lr[i] = learning_rate_value
                
        # Exponential decay
        if learning_rate_type == "exponential":
            learning_rate_value = (starting_learning_rate*math.exp(-k*i))
            lr[i] = learning_rate_value
            
        # 1/t decay
        if learning_rate_type == "1/t":
            learning_rate_value = starting_learning_rate/(1+(k*i))
            lr[i] = learning_rate_value
                
            
    # plot the cost
    plt.figure(figsize=(10,5))
    lines = plt.plot(list(costs.keys()), list(costs.values()), # first line - cost
                     lr.keys(), list(lr.values()))             # second line - learning rate
    plt.setp(lines[0], linewidth=2)
    plt.setp(lines[1], linewidth=2)    
    plt.legend(('Cost', learning_rate_type),loc='upper right')    
    plt.xlabel('iterations')
    plt.title("Learning rate vs Cost")
    plt.grid()
    
    plt.show()
    
    if learning_rate_type == None:
        return parameters
    else:
        return (parameters, lr)


def L_layer_model_orig(X, Y, layers_dims, learning_rate = 0.0075, num_iterations = 10, print_cost=False,  hidden_activation = "relu"):#lr was 0.009
    """
    Implements a L-layer neural network: [LINEAR->RELU]*(L-1)->LINEAR->SIGMOID.
    
    Arguments:
    X -- data, numpy array of shape (number of examples, num_px * num_px * 3)
    Y -- true "label" vector (containing 0 if cat, 1 if non-cat), of shape (1, number of examples)
    layers_dims -- list containing the input size and each layer size, of length (number of layers + 1).
    learning_rate -- learning rate of the gradient descent update rule
    num_iterations -- number of iterations of the optimization loop
    print_cost -- if True, it prints the cost every 100 steps
    
    Returns:
    parameters -- parameters learnt by the model. They can then be used to predict.
    """

    np.random.seed(1)
    costs = []                         # keep track of cost
    
    # Parameters initialization. (≈ 1 line of code) 
    parameters = initialize_parameters_deep(layers_dims) 
    
    # Loop (gradient descent)
    for i in range(0, num_iterations):

        # Forward propagation: [LINEAR -> RELU]*(L-1) -> LINEAR -> SIGMOID. 
        AL, caches = L_model_forward(X, parameters, hidden_activation) 
         
        # Compute cost. 
        cost = compute_cost(AL,Y) # binary cross
                
        # Backward propagation. 
        grads = L_model_backward(AL,Y,caches, hidden_activation) 
 
        # Update parameters. 
        parameters = update_parameters(parameters, grads, learning_rate) 
                
        # Print the cost every 100 training example
        if print_cost and i % 100 == 0:
            print ("Cost after iteration %i: %f" %(i, cost))
        if print_cost and i % 100 == 0:
            costs.append(cost)
            
    # plot the cost
    plt.plot(np.squeeze(costs))
    plt.ylabel('cost')
    plt.xlabel('iterations (per tens)')
    plt.title("Learning rate =" + str(learning_rate))
    plt.show()
    
    return parameters


def lr_comparison(X, 
                  Y, 
                  layers_dims,
                  learning_rate_value_sd,                    # if you chose learning rate fixed then specify the learning rate
                  learning_rate_value_ex,                    # if you chose learning rate fixed then specify the learning rate
                  learning_rate_value_1t,                    # if you chose learning rate fixed then specify the learning rate
                  learning_rate_decrease_rate,            # STEP DECAY: chose quantity to gradually decrease lr
                  learning_rate_interval_decrease,        # STEP DECAY: chose the interval to low the lr
                  starting_learning_rate,                 # EXPONENTIAL DECAY/(1/t): this is alpha0
                  k,                                      # EXPONENTIAL DECAY/(1/t): this is the exponent k
                  num_iterations,                         # number of iterations to perform
                  print_cost=False,
                  hidden_act = "relu"):#lr was 0.009


    np.random.seed(1)
    
    costs_sd = {}                         # keep track of cost
    costs_ex = {}                         # keep track of cost
    costs_1t = {}                         # keep track of cost
    
    lr_sd = {}                            # keep track of learning rate evolution
    lr_ex = {}                            # keep track of learning rate evolution
    lr_1t = {}                            # keep track of learning rate evolution
    
    lr_sd[0] = learning_rate_value_sd        # learning rate at first iteration
    lr_ex[0] = learning_rate_value_ex        # learning rate at first iteration
    lr_1t[0] = learning_rate_value_1t        # learning rate at first iteration
    
    # Parameters initialization. (≈ 1 line of code) 
    parameters_sd = initialize_parameters_deep(layers_dims)
    parameters_ex = initialize_parameters_deep(layers_dims) 
    parameters_1t = initialize_parameters_deep(layers_dims) 

    # Loop (gradient descent)
    for i in range(0, num_iterations):
        
        # STEP DECAY ANNEALING
        AL_sd, caches_sd = L_model_forward(X, parameters_sd, hidden_act)
        cost = compute_cost(AL_sd,Y) 
        grads = L_model_backward(AL_sd,Y,caches_sd, hidden_act)
        parameters_sd = update_parameters(parameters_sd, grads, learning_rate_value_sd)
        
        if print_cost and i % 50 == 0:
            costs_sd[i] = cost
        
        if i % learning_rate_interval_decrease == 0:
            # low the learning rate
            learning_rate_value_sd -= (learning_rate_value_sd*learning_rate_decrease_rate)
            # save lr value in dictonary
            lr_sd[i] = learning_rate_value_sd
        
        # EXPONENTIAL ANNEALING
        AL_ex, caches_ex = L_model_forward(X, parameters_ex, hidden_act)
        cost = compute_cost(AL_ex,Y) 
        grads = L_model_backward(AL_ex,Y,caches_ex, hidden_act)
        parameters_ex = update_parameters(parameters_ex, grads, learning_rate_value_ex)

        if print_cost and i % 50 == 0:
            costs_ex[i] = cost
        
        learning_rate_value_ex = (starting_learning_rate*np.exp(-k*i))
        lr_ex[i] = learning_rate_value_ex
        
        
        # 1/T ANNEALING
        AL_1t, caches_1t = L_model_forward(X, parameters_1t, hidden_act)
        cost = compute_cost(AL_1t,Y) 
        grads = L_model_backward(AL_1t,Y,caches_1t, hidden_act)
        parameters_1t = update_parameters(parameters_1t, grads, learning_rate_value_1t)

        if print_cost and i % 50 == 0:
            costs_1t[i] = cost
        
        learning_rate_value_1t = starting_learning_rate/(1+(k*i))
        lr_1t[i] = learning_rate_value_1t
        
    # plot the cost
    plt.figure(figsize=(10,5))
    lines = plt.plot(list(costs_sd.keys()), list(costs_sd.values()), # first line - cost
                     list(costs_ex.keys()), list(costs_ex.values()), # first line - cost
                     list(costs_1t.keys()), list(costs_1t.values())) # first line - cost
    plt.setp(lines[0], linewidth=2)
    plt.setp(lines[1], linewidth=2)
    plt.setp(lines[2], linewidth=2)
    plt.legend(('Step Decay', "Exponential", "1/t"),loc='upper right')    
    plt.xlabel('iterations')
    plt.title("Cost")
    plt.grid()    
    plt.show()
    
    # plot the cost
    plt.figure(figsize=(10,5))
    lines = plt.plot(list(lr_sd.keys()), list(lr_sd.values()), # first line - cost
                     list(lr_ex.keys()), list(lr_ex.values()), # first line - cost
                     list(lr_1t.keys()), list(lr_1t.values())) # first line - cost
    plt.setp(lines[0], linewidth=2)
    plt.setp(lines[1], linewidth=2)
    plt.setp(lines[2], linewidth=2)
    plt.legend(('Step Decay', "Exponential", "1/t"),loc='upper right')    
    plt.xlabel('iterations')
    plt.title("Learning Rate")
    plt.grid()    
    plt.show() 

    return (lr_sd, lr_ex, lr_1t)